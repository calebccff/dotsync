# Dotsync

A program to allow easy synchronisation of config files between devices that
provides the ability to modify the file on sync to set system specific options
(e.g. monitor output, resolution etc).
It could also be used to store generic config files that can generated on the
fly by reading system parameters on sync.

### **NOTE:** This program is far from complete and is not yet functional.
I will create a release when it's ready, you can enable notifications so 
you'll be notified.

# Installing
```bash
git clone https://gitlab.com/kalube/dotsync.git
cd dotsync
sudo python -m pip install -r requirements.txt #Installs requirements
./dotsync.py #This will create your config files!
```
Either run `dotsync.py` manually or use another script to automate periodic updates.

TODO: Have an argument to regenerate config files in a custom directory
TODO: Argument to specify custom config location

# Docs
## Token
This file must contain a github token with just the "gists" permission on the first line.
All other content is ignored

## Config
This file allows you the configure the profile for each machine you use this script. It is well documented
**NOTE:** Please add a file called "token" in your config folder, it should contain your github token which can be generated [here](https://github.com/settings/tokens)

## Mods
This is where the magic happens, you can define a mod for each config file you wish to sync,
this allows you to keep device specific options as well as generate them on the fly.

Each mod is a folder that matches the name defined in `config.toml`. It must contain an executable
file that starts with the word `mod`.

### Execution
Upon retreiving the file from github, your `mod` executable will be called with the
file path as the first parameter and the name of the current profile as the second.
Your mod should return with status 0 on success, any other value will be treated as
an error and will trigger an alert with `notify-send` (is this standard?) if configured.
