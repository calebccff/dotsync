#!/usr/bin/env python
### Python3 required

import toml, os, argparse
from attrdict import *
from shutil import copy2, copytree #For copying files and preserving perms

from github3 import login

CONFIG_DIR = "{}/.config/dotsync/".format(os.environ.get("HOME"))
CONFIG_FILE = "dotsync.toml"

def load_from_conf(fname, attr="r"):
  return open(CONFIG_DIR+fname, attr)


def load_config(conf=CONFIG_FILE): #Absolute path to config file
  if os.path.isfile(CONFIG_DIR+conf):
    config = toml.loads(load_from_conf(conf).read())
    return AttrDict.from_nested_dict(config) #Convert nested dictionaries to nested objects

  try: #Failed to load file, copy the default config over
    copytree(os.getcwd()+"/default_config/", CONFIG_DIR)
  except Exception as e: #Failed to copy
    #TODO: What exception is this? Deal with it properly
    print("The file \"{}\" is missing but the config directory exists, \
please manually repair the directory to match the contents of \"default_config\""
      .format(CONFIG_FILE))
    exit()
  
  print("Please configure this program by modifying the config file in \n\"{}\"".format(CONFIG_DIR))
  exit()

config = load_config()
token = load_from_conf("token").readlines()[0].strip("\n")
gh = login(token=token)
if config.gistid == "":
  cfile = toml.loads(load_from_conf(CONFIG_FILE).read())
  cfile["gistid"] = gh.create_gist(config.gistname, {}, public=False)
  toml.dumps(cfile)
files = [x for x in gists.files.values()][0].filename